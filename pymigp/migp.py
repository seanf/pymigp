#!/usr/bin/env python
# Sean Fitzgibbon
# FMRIB/WIN Analysis Group
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Python port of MIGP.

Smith, S.M., Hyvärinen, A., Varoquaux, G., Miller, K.L., & Beckmann, C.F. (2014). Group-PCA for
very large fMRI datasets. Neuroimage, 101, 738-749.

"""
import json
import logging
import os
import os.path as op
import random
import time
from subprocess import check_output

import nibabel as nb
import numpy as np
from scipy.sparse.linalg import svds, eigsh
from sys import getsizeof

LOG_FORMAT = "[%(asctime)s - %(name)s.%(funcName)s ] %(levelname)s : %(message)s"
LOG_LEVEL = logging.DEBUG

MIGP_PATH = op.dirname(op.abspath(__file__))

# with open(f'{MIGP_PATH}/default.yaml', 'r') as fpr:
#     config = yaml.safe_load(fpr)

# FSLSUB_PENAME = config['FSLSUB_PENAME']
# FSLSUB_QUEUE = config['FSLSUB_QUEUE']
# FSLSUB_TIME = config['FSLSUB_TIME']
# PYTHON_BIN = config['PYTHON_BIN']


def assert_env(env):
    if os.getenv(env) is None:
        raise RuntimeError(f"Environment variable {env} is not set.")


def _length(x):
    return np.max(x.shape)


def _feta(eta, y, sig2=1):
    bm = sig2 * (1 - np.sqrt(y)) ** 2
    bp = sig2 * (1 + np.sqrt(y)) ** 2

    inc = 1 / 1000
    teta = np.arange(start=bm, stop=bp, step=inc)

    feta = ((2 * np.pi * y * teta) ** (-1)) * np.sqrt((teta - bm) * (bp - teta))
    tmp = (teta[:, np.newaxis] @ np.ones((1, eta.shape[0]))) / (
        np.ones((teta.shape[0], 1)) @ eta[np.newaxis, :]
    ) < 1
    return np.sum(
        (0.001 * feta[:, np.newaxis] @ np.ones((1, eta.shape[0]))) * tmp, axis=0
    )


def _ifeta(eta, d1, d2):
    res = d1 * (1 - _feta(eta, d1 / d2))
    result = np.zeros(d1)
    for k in np.arange(d1):
        result[k] = eta[np.where(res >= (k + 1))[0][-1]]

    return result


def wishart_filter(W, dPCA, plot=False, threshold=1e-5):
    W = W[:dPCA, :].T

    EigS = np.sqrt(np.sum(W**2, axis=0)).T

    EigD = EigS**2
    EigD = EigD / np.max(EigD)

    EigDn1 = np.round(_length(EigD) * 0.6).astype(int)
    EigDn2 = np.round(_length(EigD) * 0.8).astype(int)

    # golden search for best-fitting spatial-DoF
    g1 = 10000
    g3 = 100000

    for i in np.arange(20):

        g2 = (g1 + g3) / 2

        EigNull = _ifeta(
            np.arange(0, 5, 0.0001), np.round(1.05 * _length(EigD)).astype(int), g2
        ).T
        EigNull = (
            EigNull
            * np.sum(EigD[EigDn2 - 1: EigDn2 + 10])
            / np.sum(EigNull[EigDn2 - 1: EigDn2 + 10])
        )

        grot = EigD[0:EigDn1] - EigNull[0:EigDn1]
        if np.min(grot) < 0:
            g1 = g2
        else:
            g3 = g2

    EigDc = EigD - EigNull[: _length(EigD)]  # subtract null eigenspectrum

    from statsmodels.nonparametric.smoothers_lowess import lowess

    grot = lowess(
        EigDc[49:],
        np.arange(_length(EigDc[49:])),
        frac=50 / _length(EigDc),
        is_sorted=True,
        it=0,
    )[:, 1].ravel()

    if not np.any(np.abs(grot) < threshold):
        raise RuntimeError(
            f"No smoothed eigenvalues < threshold ({threshold}).  Try raising threshold."
        )

    i = np.where(np.abs(grot) < threshold)[0][0] + 50 - 10

    n = 1 + _length(EigDc) - i
    EigDc[i - 1:] = (1 - np.arange(1, n + 1) / n) ** 2 * grot[i - 50]
    EigSc = np.sqrt(np.abs(EigDc))

    EigScn = (EigSc > 0) * EigSc / (EigS / EigS[0])  # correction factor

    if plot:
        pass

        # import matplotlib.pyplot as plt

        # fig, ax = plt.subplots(1, 3, figsize=(15, 5))

        # ax[0].plot(EigNull, label="EigNull")
        # ax[0].plot(EigD, label="EigD")
        # ax[0].plot(EigDc, label="EigDc")

        # ax[1].plot(EigSc, label="EigSc")
        # ax[1].plot(EigScn, label="EigScn")

        # ax[2].plot(EigS, label="EigS")
        # ax[2].plot(EigS * EigScn, label="EigS*EigScn")

        # for a in ax:
        #     a.grid()
        #     a.legend()

        # plt.show()

    V = (W @ np.diag(EigScn)).T
    D = EigS * EigScn

    return V, D


def _demean(d, dim=0):
    d = d - np.mean(d, axis=dim)
    return d


def _ntime(d):
    if isinstance(d, nb.Cifti2Image):
        return d.shape[0]
    elif isinstance(d, (nb.Nifti1Image, nb.Nifti2Image)):
        return d.shape[-1]
    elif isinstance(d, nb.GiftiImage):
        return _gii_shape(d)[-1]
    else:
        raise RuntimeError(f"unknown dtype {type(d)}")


def _gii_shape(d):
    return np.stack(d.agg_data(), axis=1).shape


def _get_fdata(d, mask):
    # return [Time x Voxels]
    if isinstance(d, nb.Cifti2Image):
        return d.get_fdata(caching="unchanged")
    elif isinstance(d, (nb.Nifti1Image, nb.Nifti2Image)):
        if mask is None:
            raise RuntimeError("brain mask is required if inputs are nifti")
        else:
            mask = nb.load(mask)
            mask = mask.get_fdata(caching="unchanged") != 0.0
        return d.get_fdata(caching="unchanged")[mask].T
    elif isinstance(d, nb.GiftiImage):
        return np.stack(d.agg_data(), axis=1).T
    else:
        raise RuntimeError(f"unknown dtype {type(d)}")


def _migp_reduce_w(W, dPCA):
    logger = logging.getLogger(__name__)

    logger.info(f"Reducing data matrix (W) to a {dPCA} dimensional subspace")

    logger.debug(f"W shape: {W.shape}")
    logger.debug(f"W size: {getsizeof(W) / 1000000}MB")

    wwt = np.dot(W, W.T)

    logger.debug(f"W*W' shape: {wwt.shape}")
    logger.debug(f"W*W' size: {getsizeof(wwt) / 1000000}MB")

    # TODO: check rtol and atol are appropriate (atol should perhaps be 0.0)
    if not check_symmetric(wwt, rtol=1e-05, atol=1e-08):
        raise RuntimeError("W.W^T is not symmetric")

    # get the top temporal eigenvectors of W
    # eigsh requires a real symmetric matrix
    vv, uu = eigsh(wwt, dPCA)

    if np.iscomplexobj(uu):
        raise RuntimeError("Outputs of eigs is complex!")

    # sort eigenvectors/values in descending magnitude of eigenvector
    idx = np.argsort(vv)[::-1]
    uu = uu[:, idx]

    # multiply the eigenvectors into W to get weighted spatial eigenvectors
    return np.dot(uu.T, W)


def _migp_worker(json):
    t0 = time.time()

    args = json2dict(json)
    image_list = args["input_filenames"]
    brainmask = args["mask"]
    dPCA = int(args["dPCA_internal"])
    sep_vn = args["sep_vn"]
    out_name = args["output_filename"]
    idx = int(args["index"])
    logdir = args["logdir"]

    n_list = len(image_list)

    # setup logging
    logging.basicConfig(
        filename=f"{logdir}/pymigp_worker{idx:02n}.log",
        level=LOG_LEVEL,
        format=LOG_FORMAT,
    )
    logger = logging.getLogger(__name__)
    logger.info(f"pyMIGP Worker {idx:02n} (pid={os.getpid()})")
    logger.info(f'Input files: {", ".join(image_list)}')

    # load images
    # image_list = [nb.load(f) for f in image_list]

    # loop through input files/subjects
    for i, fname in enumerate(image_list):

        f = nb.load(fname)

        # read data
        logger.info(f"({i + 1} of {n_list}) Reading data file {f.get_filename()}")
        d0 = _get_fdata(f, brainmask)

        logger.debug(f"Input file shape: {d0.shape}")
        logger.debug(f"Input file size: {getsizeof(d0)/1000000}MB")

        # demean
        logger.info("Removing mean image")
        d0 = _demean(d0)

        # perform subject-level variance normalisation
        if sep_vn:
            logger.info("Normalising by voxel-wise variance")

            if os.getenv("MIGP_TRIVIAL_VARNORM") == "TRUE":
                logger.info("Using TRIVIAL voxel-wise variance normalisation")
                stddevs = np.std(d0, axis=0)
                logger.info(f"N stddevs < 0.001: {np.sum(stddevs<0.001)}")
                stddevs = np.maximum(stddevs, 0.001)
            else:
                [uu, ss, vt] = svds(d0, k=np.minimum(30, d0.shape[0] - 1))
                vt[np.abs(vt) < (2.3 * np.std(vt))] = 0
                stddevs = np.maximum(
                    np.std(d0 - (uu @ np.diag(ss) @ vt), axis=0), 0.001
                )

            d0 = d0 / stddevs

        if i == 0:
            W = _demean(d0)
        elif i > 0:
            # concat W with the next subject
            W = np.concatenate((W, _demean(d0)), axis=0)

            # reduce W to dPCA_int eigenvectors
            if W.shape[0] - 10 > dPCA:
                W = _migp_reduce_w(W, dPCA)

        logger.info(f"Iteration time: {time.time() - t0:0.2f}s")
        t0 = time.time()

    logger.info(f"Save: {out_name}")
    np.save(out_name, W)


def _migp_merge(json):
    args = json2dict(json)

    w_list = args["worker_list"]
    brainmask = args["mask"]
    dPCA_in = int(args["dPCA_internal"])
    dPCA_out = int(args["dPCA_output"])
    out_name = args["output_filename"]
    out_mask = args["output_maskname"]
    img_type = args["image_type"]
    img_shape = args["image_shape"]
    img_pixdim = args["image_pixdim"]
    logdir = args["logdir"]
    image_ref = args["image_ref"]

    logging.basicConfig(
        filename=f"{logdir}/pymigp_merge.log", level=LOG_LEVEL, format=LOG_FORMAT
    )
    logger = logging.getLogger(__name__)
    logger.info(f"pyMIGP Merge (pid={os.getpid()})")
    logger.info(f'Input (W) files: {", ".join(w_list)}')

    W = [np.load(w0, allow_pickle=False) for w0 in w_list]

    for w0, f in zip(W, w_list):
        logger.debug(f"{op.basename(f)} shape: {w0.shape}")
        logger.debug(f"{op.basename(f)} size: {getsizeof(w0) / 1000000}MB")

    # merge if required
    if len(W) == 1:
        W = W[0]
    elif len(W) > 1:
        logger.info("Merge and reduce worker outputs")
        W = np.concatenate(W, axis=0)
        W = _migp_reduce_w(W, dPCA_in)
    else:
        raise RuntimeError("W is empty... I don't know what this means, but it is bad.")

    # test wishart
    if os.getenv("MIGP_WISHART_FILTER") == "TRUE":
        logger.info("Apply Wishart filter")
        W, _ = wishart_filter(W, dPCA_out)
        W = _migp_reduce_w(W, dPCA_out)

    # reshape and save
    if img_type == "nifti":

        img_shape = tuple(img_shape)

        brainmask = nb.load(brainmask)
        brainmask = brainmask.get_fdata(caching="unchanged") != 0.0

        d0 = np.zeros([brainmask.size, dPCA_out])
        d0[brainmask.ravel(), :] = W[:dPCA_out, :].T
        d0 = np.reshape(d0, img_shape[:3] + (dPCA_out,))
        m = brainmask
    else:
        d0 = W[:dPCA_out, :]
        # d0, m = fold(d0)

    # logger.info(f"Final shape: {d0.shape}")

    logger.info(f"Writing MIPG results to: {out_name}")
    out_name = op.abspath(out_name)
    if not op.exists(op.dirname(out_name)):
        os.makedirs(op.dirname(out_name))

    if img_type == 'cifti':

        if not out_name.endswith('.dscalar.nii'):
            out_name = out_name + '.dscalar.nii'

        logger.info(f"Final shape: {d0.shape}")

        ref = nb.load(image_ref)
        bm_full = ref.header.get_axis(1)

        scalar = nb.cifti2.ScalarAxis(name=[f'{i}' for i in np.arange(d0.shape[0])])
        nb.Cifti2Image(d0, header=(scalar, bm_full)).to_filename(out_name)

    else:

        if not out_name.endswith('.nii.gz'):
            out_name = out_name + '.nii.gz'

        # d0, m = fold(d0.T)
        logger.info(f"Final shape: {d0.shape}")

        ref = nb.load(image_ref)

        dd = nb.Nifti1Image(d0, header=ref.header, affine=ref.affine)
        # dd.header["pixdim"] = img_pixdim
        dd.to_filename(out_name)

        logger.info(f"Writing MIPG mask to: {out_mask}")
        out_mask = op.abspath(out_mask)
        if not op.exists(op.dirname(out_mask)):
            os.makedirs(op.dirname(out_mask))

        dd = nb.Nifti1Image(m.astype(np.int8), header=ref.header, affine=ref.affine)
        # dd.header["pixdim"] = img_pixdim
        dd.to_filename(out_mask)


def batch_iterator(lst, n):
    """Yield n number of sublists from lst."""
    for i in range(0, len(lst), n):
        yield lst[i: i + n]


def migp(
    image_list,
    dPCA_int,
    brainmask=None,
    out_name=None,
    workdir="./migp",
    out_mask=None,
    sep_vn=True,
    dPCA_out=None,
    n_jobs=1,
    n_threads_per_job=None,
    random_seed=None,
    worker_memory=None,
    merge_memory=None,
):
    """
    MIGP

    :param clean:
    :param n_threads_per_job:
    :param workdir:
    :param random_seed:
    :param n_jobs:
    :param out_mask:
    :param image_list:
    :param brainmask:
    :param out_name:
    :param sep_vn:
    :param dPCA_int:
    :param dPCA_out:
    :return:
    """

    assert_env('FSLDIR')

    # if n_threads_per_job is not None:
    #     os.environ["OMP_NUM_THREADS"] = str(n_threads_per_job)
    #     os.environ["OPENBLAS_NUM_THREADS"] = str(n_threads_per_job)

    # check args
    image0 = nb.load(image_list[0])

    if isinstance(image0, (nb.Nifti1Image, nb.Nifti2Image)):
        img_type = "nifti"
    elif isinstance(image0, nb.Cifti2Image):
        img_type = "cifti"
    elif isinstance(image0, nb.GiftiImage):
        img_type = "gifti"
    else:
        raise RuntimeError(f"Unsupported image type: {type(image0)}")

    # if not all([isinstance(nb.load(f), type(image0)) for f in image_list]):
    #     raise RuntimeError('cannot mix data-types in in_list')

    # set up output names
    while op.exists(workdir):
        workdir += "+"
    if not op.exists(workdir):
        os.makedirs(workdir)

    if out_name is None:
        out_name = f"{workdir}/pymigp"

    if out_mask is None:
        out_mask = f"{workdir}/pymigp_mask.nii.gz"

    logdir = f"{workdir}/logs"
    if not op.exists(logdir):
        os.makedirs(logdir)

    tmpdir = f"{workdir}/workers"
    if not op.exists(tmpdir):
        os.makedirs(tmpdir)

    #  setup logging

    logging.basicConfig(
        filename=f"{logdir}/pymigp.log", format=LOG_FORMAT, level=LOG_LEVEL
    )

    logger = logging.getLogger(__name__)
    logger.info(f"pyMIGP (pid={os.getpid()})")
    logger.info(f'Input files: {", ".join(image_list)}')
    logger.info(f"Input brain mask: {brainmask}")

    # internal number of components - typically 2-4 times number of timepoints in each run
    # if dPCA_int is None:
    #     dPCA_int = 2 * np.min([_ntime(nb.load(d)) for d in image_list]).astype(int)
    # logger.info(f'Internal number of components: {dPCA_int}')

    # number of eigenvectors to output - should be less than dPCAint and
    # more than the final ICA dimensionality
    if dPCA_out is None:
        dPCA_out = dPCA_int
    logger.info(f"Output number of components: {dPCA_out}")

    # randomise the subject order
    random.seed(random_seed)
    random.shuffle(image_list)

    # split into N batches (N=n_jobs) and process
    worker_list = []
    jid_list = []

    batch_size = np.ceil(len(image_list) / n_jobs).astype(int)
    for idx, b in enumerate(batch_iterator(image_list, batch_size)):
        # create output filename for worker
        filename = op.join(tmpdir, f"_migp_worker_W{idx:02n}.npy")
        worker_list.append(filename)

        # write args to file
        args_filename = op.join(tmpdir, f"_migp_worker_args{idx:02n}.json")
        dict2json(
            {
                "workdir": op.abspath(workdir),
                "input_filenames": [op.abspath(f) for f in b],
                "mask": op.abspath(brainmask) if brainmask is not None else brainmask,
                "dPCA_internal": int(dPCA_int),
                "sep_vn": sep_vn,
                "output_filename": op.abspath(filename),
                "index": idx,
                "logdir": op.abspath(logdir),
            },
            args_filename,
        )

        # submit _migp_worker to cluster (fsl_sub)

        PYTHON_BIN = os.getenv('PYTHON_BIN', default='python')

        cmd = f"{PYTHON_BIN} {MIGP_PATH}/cli/migp_helper.py WORKER {args_filename}"
        jid = _submit(
            cmd,
            queue=os.getenv('FSLSUB_QUEUE', default='long.q'),
            logdir=op.join(logdir, "fsl_sub"),
            job_name=f"pymigp_worker{idx:02n}",
            multi_threaded=n_threads_per_job,
            memory=worker_memory,
            pename=os.getenv('FSLSUB_PENAME', default='openmp'),
        )
        jid_list.append(jid)

    # merge & save outputs #

    # write args to file
    args_filename = op.join(tmpdir, "_migp_merge_args.json")
    dict2json(
        {
            "workdir": op.abspath(workdir),
            "worker_list": [op.abspath(w) for w in worker_list],
            "mask": op.abspath(brainmask) if brainmask is not None else brainmask,
            "dPCA_internal": int(dPCA_int),
            "dPCA_output": int(dPCA_out),
            "output_filename": op.abspath(out_name),
            "output_maskname": op.abspath(out_mask),
            "logdir": op.abspath(logdir),
            "image_type": img_type,
            "image_shape": list(image0.shape) if img_type == "nifti" else None,
            "image_pixdim": image0.header["pixdim"].tolist() if img_type == "nifti" else None,
            "image_ref": image_list[0],
        },
        args_filename,
    )

    # submit _migp_merge to cluster (fsl_sub)
    cmd = f"{PYTHON_BIN} {MIGP_PATH}/cli/migp_helper.py MERGE {args_filename}"
    jid = _submit(
        cmd,
        queue=os.getenv('FSLSUB_QUEUE', default='long.q'),
        logdir=op.join(logdir, "fsl_sub"),
        job_name="pymigp_merge",
        wait_for=jid_list,
        multi_threaded=n_threads_per_job,
        memory=merge_memory,
        pename=os.getenv('FSLSUB_PENAME', default='openmp'),
    )

    # # cleanup _migp_worker files if requested
    # if clean:
    #     _submit(
    #         f"rm -r {tmpdir}",
    #         queue="veryshort.q",
    #         job_name="pymigp_cleanup",
    #         wait_for=jid,
    #         logdir=op.join(logdir, "fsl_sub"),
    #     )


def _submit(
    cmd,
    queue="long.q",
    wait_for=None,
    logdir=None,
    multi_threaded=None,
    job_name=None,
    memory=None,
    pename="openmp",
    time=None,
):
    fsldir = os.environ["FSLDIR"]
    fslsub = op.join(fsldir, "bin", "fsl_sub")

    fslsub = [fslsub, "-q", queue]
    if logdir is not None:
        fslsub += ["-l", logdir]
    if multi_threaded is not None:
        if multi_threaded > 1:
            fslsub += ["-s", f"{pename},{multi_threaded}"]
            fslsub += ["--export", f"OMP_NUM_THREADS={multi_threaded}"]
            fslsub += ["--export", f"OPENBLAS_NUM_THREADS={multi_threaded}"]
            fslsub += ["--export", f"MKL_NUM_THREADS={multi_threaded}"]
            # fslsub += ["--export", f"GOTO_NUM_THREADS={multi_threaded}"]
            # fslsub += ["--export", f"MKL_DOMAIN_NUM_THREADS={multi_threaded}"]
    if wait_for is not None:
        wait_for = ",".join(wait_for) if isinstance(wait_for, list) else wait_for
        fslsub += ["-j", wait_for]
    if job_name is not None:
        fslsub += ["-N", job_name]
    if memory is not None:
        fslsub += ["-R", f"{memory}"]
    if time is not None:
        fslsub += ["-T", f"{time}"]

    if isinstance(cmd, str):
        cmd = cmd.split()
    fslsub += cmd

    return run(fslsub)


def run(cmd):
    logger = logging.getLogger(__name__)

    if type(cmd) is list:
        str_cmd = " ".join(cmd)
    else:
        str_cmd = cmd
        cmd = str_cmd.split()

    logger.info("RUNNING: " + str_cmd)
    job_out = check_output(cmd)

    return job_out.decode("utf-8").strip()


def fold(d):
    n_t = d.shape[1]
    n_v = d.shape[0]

    dim = np.ceil(n_v / 100.0).astype(int)

    d0 = np.zeros((100 * dim, n_t))
    d0[:n_v, :] = d
    d0 = d0.reshape((10, 10, dim, n_t))

    mask = np.ones((100 * dim))
    mask[n_v:] = 0
    mask = mask.reshape((10, 10, dim))

    return d0, mask.astype(bool)


def check_symmetric(a, rtol=1e-05, atol=1e-08):
    """
    Check matrix is symmetric.
    https://stackoverflow.com/questions/42908334/checking-if-a-matrix-is-symmetric-in-numpy

    :param a:
    :param rtol:
    :param atol:
    :return:
    """
    return np.allclose(a, a.T, rtol=rtol, atol=atol)


def dict2json(dict, jsonfile, indent=4):
    """Write dictionary to json file."""
    with open(jsonfile, "w") as outfile:
        json.dump(dict, outfile, indent=indent)


def json2dict(jsonfile):
    """Read dictionary from json file."""
    with open(jsonfile, "r") as infile:
        d = json.load(infile)
    return d
