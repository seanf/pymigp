#!/usr/bin/env python
# Sean Fitzgibbon
# FMRIB/WIN Analysis Group
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Python port of MIGP - Helper CLI for job submission.
"""
import argparse
import sys
from pymigp import migp

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description='CLI for MIGP helper functions.  Used for HPC job submission. Not really meant for users.'
    )
    subparsers = parser.add_subparsers()

    # --- worker

    parser_worker = subparsers.add_parser(
        'WORKER',
        description='Run MIGP worker',
        formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=55, width=100)
    )

    parser_worker.add_argument(
        'json',
        help='JSON file with _migp_worker args',
        type=str,
    )

    parser_worker.set_defaults(method='worker')

    # --- merge

    parser_merge = subparsers.add_parser(
        'MERGE',
        description='Run MIGP merge',
        formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=55, width=100)
    )

    parser_merge.add_argument(
        'json',
        help='JSON file with _migp_merge args',
        type=str,
    )

    parser_merge.set_defaults(method='merge')

    # --- parse args

    if len(sys.argv) == 1:
        parser.parse_args(['-h'])
    else:
        args = vars(parser.parse_args())

    # print(args)

    method = args.pop('method')

    if method == 'worker':
        migp._migp_worker(args['json'])
    elif method == 'merge':
        migp._migp_merge(args['json'])
    else:
        raise RuntimeError(f'Unknown method: {method}')
