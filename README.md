# pyMIGP

This package contains a python port for the MIGP algorithm *(Smith et al., 2014)*.

For group ICA,  `melodic` uses multi-session temporal concatenation. This will perform a single 2D ICA run on the concatenated data matrix (obtained by stacking all 2D data matrices of every single data set on top of each other).

![temporal concatenation](docs/images/concat_diag.png)

Resulting in **high dimension** datasets!

Furthermore, with ICA we are typically only interested in a comparitively low dimension decomposition so that we can capture spatially extended networks.

Therefore the first step is to reduce the dimensionality of the data.  This can be achieved in a number of ways, but `melodic`, by default, uses `MIGP`.

> MIGP is an incremental approach that aims to provide a very close approximation to full temporal concatenation followed by PCA, but without the large memory requirements *(Smith et al., 2014)*.

Essentially, MIGP stacks the datasets incrementally in the temporal dimension, and whenever the temporal dimension exceeds a specified size, a PCA-based temporal reduction is performed.

> MIGP does not increase at all in memory requirement with increasing numbers of subjects, no large matrices are ever formed, and the computation time scales linearly with the number of subjects. It is easily parallelisable, simply by applying the approach in parallel to subsets of subjects, and then combining across these with the same “concatenate and reduce” approach described above *(Smith et al., 2014)*.

## Installation
```shell
# install development version from gitlab with pip
pip install git+https://git.fmrib.ox.ac.uk/seanf/pymigp.git

# OR, clone locally and install with pip
git clone https://git.fmrib.ox.ac.uk/seanf/pymigp.git
pip install -e pymigp
```

## Primary Citation
Smith, S. M., Hyvärinen, A., Varoquaux, G., Miller, K. L., & Beckmann, C. F. (2014). Group-PCA for very large fMRI datasets. NeuroImage, 101, 738–749. https://doi.org/10.1016/j.neuroimage.2014.07.051



