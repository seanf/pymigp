# MIGP

For group ICA,  `melodic` uses multi-session temporal concatenation. This will perform a single 2D ICA run on the concatenated data matrix (obtained by stacking all 2D data matrices of every single data set on top of each other).

![temporal concatenation](images/concat_diag.png)

Resulting in **high dimension** datasets!

Furthermore, with ICA we are typically only interested in a comparitively low dimension decomposition so that we can capture spatially extended networks.

Therefore the first step is to reduce the dimensionality of the data.  This can be achieved in a number of ways, but `melodic`, by default, uses `MIGP`.

> MIGP is an incremental approach that aims to provide a very close approximation to full temporal concatenation followed by PCA, but without the large memory requirements *(Smith et al., 2014)*.

Essentially, MIGP stacks the datasets incrementally in the temporal dimension, and whenever the temporal dimension exceeds a specified size, a PCA-based temporal reduction is performed.

> MIGP does not increase at all in memory requirement with increasing numbers of subjects, no large matrices are ever formed, and the computation time scales linearly with the number of subjects. It is easily parallelisable, simply by applying the approach in parallel to subsets of subjects, and then combining across these with the same “concatenate and reduce” approach described above *(Smith et al., 2014)*.

## This notebook

This notebook will download an open fMRI dataset (~50MB) for use in the pyMIGP demo, regresses confounds from the data, performs spatial smoothing with 10mm FWHM, and then runs group `melodic` with `MIGP`.

* [Fetch the data](#download-the-data)
* [Clean the data](#clean-the-data)
* [Run `melodic`](#run-melodic)
* [Plot group ICs](#plot-group-ics)

Firstly we will import the necessary packages for this notebook:                           


```python
from nilearn import datasets
from nilearn import image
from nilearn import plotting
import nibabel as nb
import numpy as np
import os.path as op
import os
import glob
import matplotlib.pyplot as plt
```

<a class="anchor" id="download-the-data"></a>
## Fetch the data

This data is a derivative from the [COBRE](http://fcon_1000.projects.nitrc.org/indi/retro/cobre.html) sample found in the International Neuroimaging Data-sharing Initiative, originally released under Creative Commons - Attribution Non-Commercial.

It comprises 10 preprocessed resting-state fMRI selected from 72 patients diagnosed with schizophrenia and 74 healthy controls (6mm isotropic, TR=2s, 150 volumes).

Create a directory in the users home directory to store the downloaded data:


```python
data_dir = op.expanduser('~/nilearn_data')

if not op.exists(data_dir):
    os.makedirs(data_dir)
```

Download the data (if not already downloaded):

> **Note:** We use a method from [`nilearn`](https://nilearn.github.io/index.html) called [`fetch_cobre`](https://nilearn.github.io/modules/generated/nilearn.datasets.fetch_cobre.html) to download the fMRI data


```python
d = datasets.fetch_cobre(data_dir=data_dir)    
```

    /Users/seanf/miniconda3/envs/dhcp/lib/python3.7/site-packages/numpy/lib/npyio.py:2372: VisibleDeprecationWarning: Reading unicode strings without specifying the encoding argument is deprecated. Set the encoding, use None for the system default.
      output = genfromtxt(fname, **kwargs)


<a class="anchor" id="clean-the-data"></a>
## Clean the data

Regress confounds from the data and to spatially smooth the data with a gaussian filter of 10mm FWHM.

> **Note:**
> 1. We use [`clean_img`](https://nilearn.github.io/modules/generated/nilearn.image.clean_img.html) from the [`nilearn`](https://nilearn.github.io/index.html) package to regress confounds from the data
> 2. We use [`smooth_img`](https://nilearn.github.io/modules/generated/nilearn.image.smooth_img.html) from the [`nilearn`](https://nilearn.github.io/index.html) package to spatially smooth the data



```python
# Create a list of filenames for cleaned and smoothed data
clean = [f.replace('.nii.gz', '_clean.nii.gz') for f in d.func]
smooth = [f.replace('.nii.gz', '_clean_smooth.nii.gz') for f in d.func]

# loop through each subject, regress confounds and smooth
for img, cleaned, smoothed, conf in zip(d.func, clean, smooth, d.confounds):
    print(f'{img}: regress confounds: ', end='')
    image.clean_img(img, confounds=conf).to_filename(cleaned)
    print(f'smooth.')
    image.smooth_img(img, 10).to_filename(smoothed)
```

    /Users/seanf/nilearn_data/cobre/fmri_0040046.nii.gz: regress confounds: smooth.
    /Users/seanf/nilearn_data/cobre/fmri_0040002.nii.gz: regress confounds: smooth.
    /Users/seanf/nilearn_data/cobre/fmri_0040117.nii.gz: regress confounds: smooth.
    /Users/seanf/nilearn_data/cobre/fmri_0040145.nii.gz: regress confounds: smooth.
    /Users/seanf/nilearn_data/cobre/fmri_0040000.nii.gz: regress confounds: smooth.
    /Users/seanf/nilearn_data/cobre/fmri_0040061.nii.gz: regress confounds: smooth.
    /Users/seanf/nilearn_data/cobre/fmri_0040090.nii.gz: regress confounds: smooth.
    /Users/seanf/nilearn_data/cobre/fmri_0040113.nii.gz: regress confounds: smooth.
    /Users/seanf/nilearn_data/cobre/fmri_0040121.nii.gz: regress confounds: smooth.
    /Users/seanf/nilearn_data/cobre/fmri_0040020.nii.gz: regress confounds: smooth.


To run ```melodic``` we will need a brain mask in MNI152 space at the same resolution as the fMRI.  

> **Note:**
> 1. We use [`load_mni152_brain_mask`](https://nilearn.github.io/modules/generated/nilearn.datasets.load_mni152_brain_mask.html) from the [`nilearn`](https://nilearn.github.io/index.html) package to load the MNI152 mask
> 2. We use [`resample_to_img`](https://nilearn.github.io/modules/generated/nilearn.image.resample_to_img.html) from the [`nilearn`](https://nilearn.github.io/index.html) package to resample the mask to the resolution of the fMRI 
> 3. We use [`math_img`](https://nilearn.github.io/modules/generated/nilearn.image.math_img.html) from the [`nilearn`](https://nilearn.github.io/index.html) package to binarize the resample mask
> 4. The mask is plotted using [`plot_anat`](https://nilearn.github.io/modules/generated/nilearn.plotting.plot_anat.html) from the [`nilearn`](https://nilearn.github.io/index.html) package


```python
# load a single fMRI dataset (func0)
func0 = nb.load(d.func[0].replace('.nii.gz', '_clean_smooth.nii.gz'))

# load MNI153 brainmask, resample to func0 resolution, binarize, and save to nifti
mask = datasets.load_mni152_brain_mask()
mask = image.resample_to_img(mask, func0)
mask = image.math_img('img > 0.5', img=mask)
mask.to_filename(op.join(data_dir, 'brain_mask.nii.gz'))

# plot brainmask to make sure it looks OK
disp = plotting.plot_anat(image.index_img(func0, 0))
disp.add_contours(mask, threshold=0.5)
```

![png](images/output_9_1.png)


<a class="anchor" id="run-melodic"></a>
### Run ```melodic```

Generate a command line string and run group ```melodic``` on the smoothed fMRI with a dimension of 10 components:


```python
# generate melodic command line string
melodic_cmd = f"melodic -i {','.join(smooth)} --mask={op.join(data_dir, 'brain_mask.nii.gz')} -d 10 -v --sep_vn -o cobre.gica"
print(melodic_cmd)
```

    melodic -i /Users/seanf/nilearn_data/cobre/fmri_0040046_clean_smooth.nii.gz,/Users/seanf/nilearn_data/cobre/fmri_0040002_clean_smooth.nii.gz,/Users/seanf/nilearn_data/cobre/fmri_0040117_clean_smooth.nii.gz,/Users/seanf/nilearn_data/cobre/fmri_0040145_clean_smooth.nii.gz,/Users/seanf/nilearn_data/cobre/fmri_0040000_clean_smooth.nii.gz,/Users/seanf/nilearn_data/cobre/fmri_0040061_clean_smooth.nii.gz,/Users/seanf/nilearn_data/cobre/fmri_0040090_clean_smooth.nii.gz,/Users/seanf/nilearn_data/cobre/fmri_0040113_clean_smooth.nii.gz,/Users/seanf/nilearn_data/cobre/fmri_0040121_clean_smooth.nii.gz,/Users/seanf/nilearn_data/cobre/fmri_0040020_clean_smooth.nii.gz --mask=/Users/seanf/nilearn_data/brain_mask.nii.gz -d 10 -v --sep_vn -o cobre.gica



```python
# run melodic
! {melodic_cmd}
```

    
    Melodic Version 3.15
    
    Melodic results will be in cobre.gica
    
    Mask provided : /Users/seanf/nilearn_data/brain_mask.nii.gz
    
    Randomising input file order
    Reading data file /Users/seanf/nilearn_data/cobre/fmri_0040090_clean_smooth  ...  done
      Estimating data smoothness ...  done 
      Removing mean image ... done
      Normalising by voxel-wise variance ... done
    Reading data file /Users/seanf/nilearn_data/cobre/fmri_0040046_clean_smooth  ...  done
      Removing mean image ... done
      Normalising by voxel-wise variance ... done
    Reading data file /Users/seanf/nilearn_data/cobre/fmri_0040145_clean_smooth  ...  done
      Removing mean image ... done
      Normalising by voxel-wise variance ... done
    Reading data file /Users/seanf/nilearn_data/cobre/fmri_0040061_clean_smooth  ...  done
      Removing mean image ... done
      Normalising by voxel-wise variance ... done
      Reducing data matrix to a  299 dimensional subspace 
    Reading data file /Users/seanf/nilearn_data/cobre/fmri_0040113_clean_smooth  ...  done
      Removing mean image ... done
      Normalising by voxel-wise variance ... done
    Reading data file /Users/seanf/nilearn_data/cobre/fmri_0040121_clean_smooth  ...  done
      Removing mean image ... done
      Normalising by voxel-wise variance ... done
      Reducing data matrix to a  299 dimensional subspace 
    Reading data file /Users/seanf/nilearn_data/cobre/fmri_0040000_clean_smooth  ...  done
      Removing mean image ... done
      Normalising by voxel-wise variance ... done
    Reading data file /Users/seanf/nilearn_data/cobre/fmri_0040002_clean_smooth  ...  done
      Removing mean image ... done
      Normalising by voxel-wise variance ... done
      Reducing data matrix to a  299 dimensional subspace 
    Reading data file /Users/seanf/nilearn_data/cobre/fmri_0040117_clean_smooth  ...  done
      Removing mean image ... done
      Normalising by voxel-wise variance ... done
    Reading data file /Users/seanf/nilearn_data/cobre/fmri_0040020_clean_smooth  ...  done
      Removing mean image ... done
      Normalising by voxel-wise variance ... done
      Reducing data matrix to a  299 dimensional subspace 
    
    Excluding voxels with constant value ... done
      Normalising by voxel-wise variance ... done
    
      Data size : 299 x 8441
    
    Starting PCA  ... done
    Start whitening using  10 dimensions ... 
      retaining 44.222 percent of the variability 
     ... done
    
    Starting ICA estimation using concat
    
      Step no. 1 change : 0.41143
      Step no. 2 change : 0.0350724
      Step no. 3 change : 0.0127465
      Step no. 4 change : 0.0103296
      Step no. 5 change : 0.00719128
      Step no. 6 change : 0.006222
      Step no. 7 change : 0.00637964
      Step no. 8 change : 0.00619995
      Step no. 9 change : 0.00644299
      Step no. 10 change : 0.00869997
      Step no. 11 change : 0.0105905
      Step no. 12 change : 0.00888758
      Step no. 13 change : 0.00466421
      Step no. 14 change : 0.00202545
      Step no. 15 change : 0.000912437
      Step no. 16 change : 0.000487308
      Step no. 17 change : 0.000316136
      Step no. 18 change : 0.000212335
      Step no. 19 change : 0.00014435
      Step no. 20 change : 9.8496e-05
      Step no. 21 change : 6.73051e-05
      Step no. 22 change : 4.60874e-05
      Convergence after 22 steps 
    
    Sorting IC maps
    
    Writing results to : 
      cobre.gica/melodic_IC
      cobre.gica/melodic_Tmodes
      cobre.gica/melodic_mix
      cobre.gica/melodic_FTmix
      cobre.gica/melodic_ICstats
      cobre.gica/mask
    ...done
    Creating report index page ...done
    
    
    Running Mixture Modelling on Z-transformed IC maps ...
      IC map 1 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 2 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 3 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 4 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 5 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 6 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 7 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 8 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 9 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 10 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
    
    Writing results to : 
      cobre.gica/melodic_IC
      cobre.gica/melodic_Tmodes
      cobre.gica/melodic_mix
      cobre.gica/melodic_FTmix
      cobre.gica/melodic_ICstats
      cobre.gica/mask
    ...done
    finished!
    


<a class="anchor" id="plot-group-ics"></a>
### Plot group ICs

Now we can load and plot the group ICs generated by ```melodic```.

This function will be used to plot ICs:


```python
def ortho_plot(d):

    N = d.shape[-1]

    fig, ax = plt.subplots(
        int(np.ceil((N/2))),2, 
        figsize=(12, N)
    )

    for img, ax0 in zip(image.iter_img(d), ax.ravel()):
        coord = plotting.find_xyz_cut_coords(img, activation_threshold=3.5)
        plotting.plot_stat_map(img, cut_coords=coord, vmax=10, axes=ax0)
        
    return fig

def lightbox_plot(d, k=5):
    
    N = d.shape[-1]

    fig, ax = plt.subplots(N, 1, figsize=(k*2, N*2))

    for img, ax0 in zip(image.iter_img(d), ax):
        plotting.plot_stat_map(img, cut_coords=k, vmax=10, axes=ax0, display_mode='z')
        
    return fig
    
```

Hopefully you can see some familiar looking RSN spatial patterns:


```python
# Load ICs
ics = nb.load('cobre.gica/melodic_IC.nii.gz')

# plot
fig = ortho_plot(ics)
```


![png](images/output_16_0.png)



```python
# plot
fig = lightbox_plot(ics, k=7)
```


![png](images/output_17_0.png)
