# pyMIGP Tutorial

This notebook will provide a brief tutorial for using the python interface for `pyMIGP`.

`pyMIGP` is run prior to `melodic` ICA.  It will concatenate and dimension reduce all the input fMRI, and then produce a single dimension reduced file for use in `melodic`.

[[_TOC_]]

## Basic Usage 

Firstly, import the libraries necessary for this notebook.


```python
import glob
import nibabel as nb
import numpy as np
import matplotlib.pyplot as plt
from nilearn import plotting
from nilearn import image
import os.path as op
import migp
import shutil
```

Next we need to make a list of input fMRI files.  The input type could be nifti, cifti, or gifti.

This could be achieved by globbing, for example:


```python
data_dir = op.expanduser('~/nilearn_data')
in_list = [f for f in glob.glob(f'{data_dir}/cobre/fmri_*_smooth.nii.gz')]

in_list
```




    ['/Users/seanf/nilearn_data/cobre/fmri_0040121_clean_smooth.nii.gz',
     '/Users/seanf/nilearn_data/cobre/fmri_0040002_clean_smooth.nii.gz',
     '/Users/seanf/nilearn_data/cobre/fmri_0040000_clean_smooth.nii.gz',
     '/Users/seanf/nilearn_data/cobre/fmri_0040046_clean_smooth.nii.gz',
     '/Users/seanf/nilearn_data/cobre/fmri_0040020_clean_smooth.nii.gz',
     '/Users/seanf/nilearn_data/cobre/fmri_0040061_clean_smooth.nii.gz',
     '/Users/seanf/nilearn_data/cobre/fmri_0040117_clean_smooth.nii.gz',
     '/Users/seanf/nilearn_data/cobre/fmri_0040145_clean_smooth.nii.gz',
     '/Users/seanf/nilearn_data/cobre/fmri_0040090_clean_smooth.nii.gz',
     '/Users/seanf/nilearn_data/cobre/fmri_0040113_clean_smooth.nii.gz']



Or it could be read from a text file, for example:


```python
with open('input_files.txt', 'r') as f:
    in_list = [l.strip() for l in f.readlines()]
    
in_list
```




    ['/Users/seanf/nilearn_data/cobre/fmri_0040121_clean_smooth.nii.gz',
     '/Users/seanf/nilearn_data/cobre/fmri_0040002_clean_smooth.nii.gz',
     '/Users/seanf/nilearn_data/cobre/fmri_0040000_clean_smooth.nii.gz',
     '/Users/seanf/nilearn_data/cobre/fmri_0040046_clean_smooth.nii.gz',
     '/Users/seanf/nilearn_data/cobre/fmri_0040020_clean_smooth.nii.gz',
     '/Users/seanf/nilearn_data/cobre/fmri_0040061_clean_smooth.nii.gz',
     '/Users/seanf/nilearn_data/cobre/fmri_0040117_clean_smooth.nii.gz',
     '/Users/seanf/nilearn_data/cobre/fmri_0040145_clean_smooth.nii.gz',
     '/Users/seanf/nilearn_data/cobre/fmri_0040090_clean_smooth.nii.gz',
     '/Users/seanf/nilearn_data/cobre/fmri_0040113_clean_smooth.nii.gz']



If the input type is nifti, then a brain mask will also be required.


```python
in_mask = f'{data_dir}/brain_mask.nii.gz'
```

This call will run MIGP on the fMRI in the `in_list` with the brain mask `in_mask`.

`dPCA_int` is dimensionality of internal PCA.  This is recommended to be 2-4 times number of timepoints in each fMRI.

`dPCA_out` tells `pyMIGP` to only return the first `n` PCA components.  If not set, then `dPCA_out` will default to be the same as `dPCA_in`.

In this invocation, MIGP will be sent to the cluster as a single job using only a single thread.
> Note: this will only succeed if `fsl_sub` is properly configured for your cluster, **AND** if `fsl_sub` understands the queue `long.q`

> Note: the default output working directory will be `./migp` and the default output filename will be `{workdir}/pymigp.nii.gz`.  These can be modified with the `workdir` and `out_name` args.


```python
migp.migp(
    image_list=in_list, 
    brainmask=in_mask, 
    dPCA_int=600,
    dPCA_out=150,
)
```

The output working directory should look like the tree below.  The important files are:
- `pymigp.nii.gz`: the dimension reduced data (as a nifti1 to make it easier for the subsequent melodic call)
- `pymigp_mask.nii.gz`: the mask for the data (required for melodic)


```python
! tree -L 2 migp*
```

    migp
    ├── logs
    │   ├── fsl_sub
    │   ├── pymigp.log
    │   ├── pymigp_merge.log
    │   └── pymigp_worker00.log
    ├── mpymigp.nii.gz
    ├── mpymigp_mask.nii.gz
    └── workers
        ├── _migp_merge_args.json
        ├── _migp_worker_W00.npy
        └── _migp_worker_args00.json
    
    6 directories, 18 files


Now you can call `melodic` on the dimension reduced data:


```bash
%%bash
melodic -i migp/pymigp.nii.gz --mask=migp/pymigp_mask.nii.gz -d 10 -v --varnorm --keep_meanvol --nobet --disableMigp -o pymigp.gica
```

    
    Melodic Version 3.15
    
    Melodic results will be in pymigp.gica
    
    Mask provided : migp/pymigp_mask.nii.gz
    
    Reading data file migp/pymigp  ...  done
      Estimating data smoothness ...  done 
    Excluding voxels with constant value ... done
    
      Data size : 150 x 8441
    
    Starting PCA  ... done
    Start whitening using  10 dimensions ... 
      retaining 45.9026 percent of the variability 
     ... done
    
    Starting ICA estimation using symm
    
      Step no. 1 change : 0.55188
      Step no. 2 change : 0.0674586
      Step no. 3 change : 0.0437635
      Step no. 4 change : 0.0203176
      Step no. 5 change : 0.00783094
      Step no. 6 change : 0.0100134
      Step no. 7 change : 0.0109836
      Step no. 8 change : 0.00982255
      Step no. 9 change : 0.00699965
      Step no. 10 change : 0.00407795
      Step no. 11 change : 0.00215931
      Step no. 12 change : 0.00154711
      Step no. 13 change : 0.00198802
      Step no. 14 change : 0.00291335
      Step no. 15 change : 0.00431383
      Step no. 16 change : 0.00572033
      Step no. 17 change : 0.00614712
      Step no. 18 change : 0.00511095
      Step no. 19 change : 0.00344549
      Step no. 20 change : 0.00215476
      Step no. 21 change : 0.0014449
      Step no. 22 change : 0.00110732
      Step no. 23 change : 0.000946673
      Step no. 24 change : 0.000855957
      Step no. 25 change : 0.000784474
      Step no. 26 change : 0.000724656
      Step no. 27 change : 0.000688544
      Step no. 28 change : 0.000636627
      Step no. 29 change : 0.000574126
      Step no. 30 change : 0.000506729
      Step no. 31 change : 0.000439269
      Step no. 32 change : 0.00037519
      Step no. 33 change : 0.000316575
      Step no. 34 change : 0.000264423
      Step no. 35 change : 0.000218984
      Step no. 36 change : 0.000180037
      Step no. 37 change : 0.000147093
      Step no. 38 change : 0.000119534
      Step no. 39 change : 9.66938e-05
      Step no. 40 change : 7.79165e-05
      Step no. 41 change : 6.2585e-05
      Step no. 42 change : 5.01401e-05
      Step no. 43 change : 4.0088e-05
      Convergence after 43 steps 
    
    Sorting IC maps
    
    Writing results to : 
      pymigp.gica/melodic_IC
      pymigp.gica/melodic_Tmodes
      pymigp.gica/melodic_mix
      pymigp.gica/melodic_FTmix
      pymigp.gica/melodic_ICstats
      pymigp.gica/mask
    ...done
    Creating report index page ...done
    
    
    Running Mixture Modelling on Z-transformed IC maps ...
      IC map 1 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 2 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 3 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 4 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 5 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 6 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 7 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 8 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 9 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 10 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
    
    Writing results to : 
      pymigp.gica/melodic_IC
      pymigp.gica/melodic_Tmodes
      pymigp.gica/melodic_mix
      pymigp.gica/melodic_FTmix
      pymigp.gica/melodic_ICstats
      pymigp.gica/mask
    ...done
    finished!
    


Now we can plot the ICs returned by `melodic` that was run on the dimension reduced data.


```python
for img0 in image.iter_img('pymigp.gica/melodic_IC.nii.gz'):
    plotting.plot_stat_map(img0, bg_img=None, cut_coords=9, vmax=10, display_mode='z')
```


![png](images/output_15_0.png)



![png](images/output_15_1.png)



![png](images/output_15_2.png)



![png](images/output_15_3.png)



![png](images/output_15_4.png)



![png](images/output_15_5.png)



![png](images/output_15_6.png)



![png](images/output_15_7.png)



![png](images/output_15_8.png)



![png](images/output_15_9.png)


## Parallel Processing

One of the advantages of MIGP is that it the processing can be split into separate jobs that can be run in parallel.

In `pyMIGP` parallel processing is achieved in two ways:
1. The job can be split into multiple workers/jobs, each worker/job is simultaneously run on the cluster, and then results of each worker/job is merged at the end
2. Within each worker/job there are processes that are multithreaded and can access multiple CPUs or CPU cores

Each of these types of parallel processing can be used separately or together.

Here we are splitting the task into two jobs with the `n_jobs` argument, and setting each job to utilise 5 threads with the `n_threads_per_job` argument.  

The `worker_memory` argument is allocating 8000 MB (8 GB) RAM to each worker/job.

> Note: this will only succeed if `fsl_sub` is properly configured for your cluster, **AND** if `fsl_sub` understands the queue `long.q`, **AND** if `fsl_sub` has a `pename` configured for `openmp`


```python
migp.migp(
    image_list=in_list, 
    brainmask=in_mask, 
    n_jobs=2, 
    n_threads_per_job=5,
    dPCA_int=600,
    dPCA_out=150,
    worker_memory=8000,
    workdir='./migp_parallel'
)
```

You can see in the output that we now have results from multiple workers, however the main output is still `pymigp.nii.gz`


```python
! tree -L 2 migp_parallel*
```

    migp_parallel
    ├── logs
    │   ├── fsl_sub
    │   ├── pymigp_merge.log
    │   ├── pymigp_worker00.log
    │   └── pymigp_worker01.log
    ├── mpymigp.nii.gz
    ├── mpymigp_mask.nii.gz
    └── workers
        ├── _migp_merge_args.json
        ├── _migp_worker_W00.npy
        ├── _migp_worker_W01.npy
        ├── _migp_worker_args00.json
        └── _migp_worker_args01.json
    
    3 directories, 10 files


Again we can run `melodic` ICA, and then plot the results:


```bash
%%bash
melodic -i migp_parallel/pymigp.nii.gz --mask=migp_parallel/pymigp_mask.nii.gz -d 10 -v --varnorm --keep_meanvol --nobet --disableMigp -o pymigp_parallel.gica
```

    
    Melodic Version 3.15
    
    Melodic results will be in pymigp_parallel.gica
    
    Mask provided : migp_parallel/pymigp_mask.nii.gz
    
    Reading data file migp_parallel/pymigp  ...  done
      Estimating data smoothness ...  done 
    Excluding voxels with constant value ... done
    
      Data size : 150 x 8441
    
    Starting PCA  ... done
    Start whitening using  10 dimensions ... 
      retaining 45.9082 percent of the variability 
     ... done
    
    Starting ICA estimation using symm
    
      Step no. 1 change : 0.618085
      Step no. 2 change : 0.0715772
      Step no. 3 change : 0.016255
      Step no. 4 change : 0.00724665
      Step no. 5 change : 0.00533723
      Step no. 6 change : 0.00483161
      Step no. 7 change : 0.00398148
      Step no. 8 change : 0.00286497
      Step no. 9 change : 0.00179339
      Step no. 10 change : 0.00136336
      Step no. 11 change : 0.00157833
      Step no. 12 change : 0.00187972
      Step no. 13 change : 0.00226139
      Step no. 14 change : 0.00266313
      Step no. 15 change : 0.0029465
      Step no. 16 change : 0.00293227
      Step no. 17 change : 0.00312498
      Step no. 18 change : 0.00321391
      Step no. 19 change : 0.00313536
      Step no. 20 change : 0.00311445
      Step no. 21 change : 0.0025818
      Step no. 22 change : 0.00183069
      Step no. 23 change : 0.00121492
      Step no. 24 change : 0.000827644
      Step no. 25 change : 0.00055828
      Step no. 26 change : 0.00040958
      Step no. 27 change : 0.000300589
      Step no. 28 change : 0.000221579
      Step no. 29 change : 0.000165988
      Step no. 30 change : 0.000127756
      Step no. 31 change : 0.000101793
      Step no. 32 change : 8.41445e-05
      Step no. 33 change : 7.19262e-05
      Step no. 34 change : 6.31409e-05
      Step no. 35 change : 5.64663e-05
      Step no. 36 change : 5.10625e-05
      Step no. 37 change : 4.64195e-05
      Convergence after 37 steps 
    
    Sorting IC maps
    
    Writing results to : 
      pymigp_parallel.gica/melodic_IC
      pymigp_parallel.gica/melodic_Tmodes
      pymigp_parallel.gica/melodic_mix
      pymigp_parallel.gica/melodic_FTmix
      pymigp_parallel.gica/melodic_ICstats
      pymigp_parallel.gica/mask
    ...done
    Creating report index page ...done
    
    
    Running Mixture Modelling on Z-transformed IC maps ...
      IC map 1 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 2 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 3 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 4 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 5 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 6 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 7 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 8 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 9 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
      IC map 10 ... 
       calculating mixture-model fit 
       re-scaling spatial maps ... 
       thresholding ... 
       alternative hypothesis test at p > 0.5
    
    Writing results to : 
      pymigp_parallel.gica/melodic_IC
      pymigp_parallel.gica/melodic_Tmodes
      pymigp_parallel.gica/melodic_mix
      pymigp_parallel.gica/melodic_FTmix
      pymigp_parallel.gica/melodic_ICstats
      pymigp_parallel.gica/mask
    ...done
    finished!
    



```python
for img0 in image.iter_img('pymigp_parallel.gica/melodic_IC.nii.gz'):
    plotting.plot_stat_map(img0, bg_img=None, cut_coords=9, vmax=10, display_mode='z')
```


![png](images/output_23_0.png)



![png](images/output_23_1.png)



![png](images/output_23_2.png)



![png](images/output_23_3.png)



![png](images/output_23_4.png)



![png](images/output_23_5.png)



![png](images/output_23_6.png)



![png](images/output_23_7.png)



![png](images/output_23_8.png)



![png](images/output_23_9.png)


