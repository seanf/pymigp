{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "# pyMIGP Tutorial\n",
    "\n",
    "This notebook will provide a brief tutorial for using the python interface for `pyMIGP`.\n",
    "\n",
    "`pyMIGP` is run prior to `melodic` ICA.  It will concatenate and dimension reduce all the input fMRI, and then produce a single dimension reduced file for use in `melodic`.\n",
    "\n",
    "## Basic Usage \n",
    "\n",
    "Firstly, import the libraries necessary for this notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import glob\n",
    "import nibabel as nb\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from nilearn import plotting\n",
    "from nilearn import image\n",
    "import os.path as op\n",
    "from pymigp import migp\n",
    "import shutil\n",
    "import os"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we need to make a list of input fMRI files.  The input type could be nifti, cifti, or gifti.\n",
    "\n",
    "This could be achieved by globbing, for example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_dir = op.expanduser('~/nilearn_data')\n",
    "in_list = [f for f in glob.glob(f'{data_dir}/development_fmri/development_fmri/*_clean_smooth.nii.gz')]\n",
    "\n",
    "in_list"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or it could be read from a text file, for example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('input_files.txt', 'r') as f:\n",
    "    in_list = [l.strip() for l in f.readlines()]\n",
    "    \n",
    "in_list"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the input type is nifti, then a brain mask will also be required."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "in_mask = f'{data_dir}/brain_mask.nii.gz'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This call will run MIGP on the fMRI in the `in_list` with the brain mask `in_mask`.\n",
    "\n",
    "`dPCA_int` is dimensionality of internal PCA.  This is recommended to be 2-4 times number of timepoints in each fMRI.\n",
    "\n",
    "`dPCA_out` tells `pyMIGP` to only return the first `n` PCA components.  If not set, then `dPCA_out` will default to be the same as `dPCA_in`.\n",
    "\n",
    "In this invocation, MIGP will be sent to the cluster as a single job using only a single thread.\n",
    "> Note: this will only succeed if `fsl_sub` is properly configured for your cluster, **AND** if `fsl_sub` understands the queue `long.q`\n",
    "\n",
    "> Note: the default output working directory will be `./migp` and the default output filename will be `{workdir}/pymigp.nii.gz`.  These can be modified with the `workdir` and `out_name` args."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "migp.migp(\n",
    "    image_list=in_list, \n",
    "    brainmask=in_mask, \n",
    "    dPCA_int=600,\n",
    "    dPCA_out=150,\n",
    "    workdir='./pymigp',\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The output working directory should look like the tree below.  The important files are:\n",
    "- `pymigp.nii.gz`: the dimension reduced data (as a nifti1 to make it easier for the subsequent melodic call)\n",
    "- `pymigp_mask.nii.gz`: the mask for the data (required for melodic)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! tree -L 2 pymigp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can call `melodic` on the dimension reduced data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! melodic -i pymigp/pymigp.nii.gz --mask=pymigp/pymigp_mask.nii.gz -d 25 -v --varnorm --keep_meanvol --nobet --disableMigp -o pymigp.gica"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can plot the ICs returned by `melodic` that was run on the dimension reduced data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def ortho_plot(d):\n",
    "\n",
    "    N = d.shape[-1]\n",
    "\n",
    "    fig, ax = plt.subplots(\n",
    "        int(np.ceil((N/2))),2, \n",
    "        figsize=(12, N)\n",
    "    )\n",
    "\n",
    "    for img, ax0 in zip(image.iter_img(d), ax.ravel()):\n",
    "        coord = plotting.find_xyz_cut_coords(img, activation_threshold=3.5)\n",
    "        plotting.plot_stat_map(img, cut_coords=coord, vmax=10, axes=ax0)\n",
    "        \n",
    "    return fig\n",
    "\n",
    "def lightbox_plot(d, k=5):\n",
    "    \n",
    "    N = d.shape[-1]\n",
    "\n",
    "    fig, ax = plt.subplots(N, 1, figsize=(k*2, N*2))\n",
    "\n",
    "    for img, ax0 in zip(image.iter_img(d), ax):\n",
    "        plotting.plot_stat_map(img, cut_coords=k, vmax=10, axes=ax0, display_mode='z')\n",
    "        \n",
    "    return fig"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load ICs\n",
    "ics = nb.load('pymigp.gica/melodic_IC.nii.gz')\n",
    "\n",
    "# plot\n",
    "fig = ortho_plot(ics)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Parallel Processing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One of the advantages of MIGP is that it the processing can be split into separate jobs that can be run in parallel.\n",
    "\n",
    "In `pyMIGP` parallel processing is achieved in two ways:\n",
    "1. The job can be split into multiple workers/jobs, each worker/job is simultaneously run on the cluster, and then results of each worker/job is merged at the end\n",
    "2. Within each worker/job there are processes that are multithreaded and can access multiple CPUs or CPU cores\n",
    "\n",
    "Each of these types of parallel processing can be used separately or together.\n",
    "\n",
    "Here we are splitting the task into two jobs with the `n_jobs` argument, and setting each job to utilise 5 threads with the `n_threads_per_job` argument.  \n",
    "\n",
    "The `worker_memory` argument is allocating 8000 MB (8 GB) RAM to each worker/job.\n",
    "\n",
    "> Note: this will only succeed if `fsl_sub` is properly configured for your cluster, **AND** if `fsl_sub` understands the queue `long.q`, **AND** if `fsl_sub` has a `pename` configured for `openmp`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "migp.migp(\n",
    "    image_list=in_list, \n",
    "    brainmask=in_mask, \n",
    "    n_jobs=2, \n",
    "    n_threads_per_job=5,\n",
    "    dPCA_int=600,\n",
    "    dPCA_out=150,\n",
    "    worker_memory=8000,\n",
    "    workdir='./migp_parallel'\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see in the output that we now have results from multiple workers, however the main output is still `pymigp.nii.gz`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! tree -L 2 migp_parallel*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again we can run `melodic` ICA, and then plot the results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "melodic -i migp_parallel/pymigp.nii.gz --mask=migp_parallel/pymigp_mask.nii.gz -d 10 -v --varnorm --keep_meanvol --nobet --disableMigp -o pymigp_parallel.gica"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for img0 in image.iter_img('pymigp_parallel.gica/melodic_IC.nii.gz'):\n",
    "    plotting.plot_stat_map(img0, bg_img=None, cut_coords=9, vmax=10, display_mode='z')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
