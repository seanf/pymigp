{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# MIGP\n",
    "\n",
    "For group ICA,  `melodic` uses multi-session temporal concatenation. This will perform a single 2D ICA run on the concatenated data matrix (obtained by stacking all 2D data matrices of every single data set on top of each other).\n",
    "\n",
    "![temporal concatenation](images/concat_diag.png)\n",
    "\n",
    "Resulting in **high dimension** datasets!\n",
    "\n",
    "Furthermore, with ICA we are typically only interested in a comparitively low dimension decomposition so that we can capture spatially extended networks.\n",
    "\n",
    "Therefore the first step is to reduce the dimensionality of the data.  This can be achieved in a number of ways, but `melodic`, by default, uses `MIGP`.\n",
    "\n",
    "> MIGP is an incremental approach that aims to provide a very close approximation to full temporal concatenation followed by PCA, but without the large memory requirements *(Smith et al., 2014)*.\n",
    "\n",
    "Essentially, MIGP stacks the datasets incrementally in the temporal dimension, and whenever the temporal dimension exceeds a specified size, a PCA-based temporal reduction is performed.\n",
    "\n",
    "> MIGP does not increase at all in memory requirement with increasing numbers of subjects, no large matrices are ever formed, and the computation time scales linearly with the number of subjects. It is easily parallelisable, simply by applying the approach in parallel to subsets of subjects, and then combining across these with the same “concatenate and reduce” approach described above *(Smith et al., 2014)*.\n",
    "\n",
    "## This notebook\n",
    "\n",
    "This notebook will download an open fMRI dataset (~50MB) for use in the pyMIGP demo, regresses confounds from the data, performs spatial smoothing with 10mm FWHM, and then runs group `melodic` with `MIGP`.\n",
    "\n",
    "* [Fetch the data](#download-the-data)\n",
    "* [Clean the data](#clean-the-data)\n",
    "* [Run `melodic`](#run-melodic)\n",
    "* [Plot group ICs](#plot-group-ics)\n",
    "\n",
    "Firstly we will import the necessary packages for this notebook:                           "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from nilearn import datasets\n",
    "from nilearn import image\n",
    "from nilearn import plotting\n",
    "import nibabel as nb\n",
    "import numpy as np\n",
    "import os.path as op\n",
    "import os\n",
    "import glob\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"download-the-data\"></a>\n",
    "## Fetch the data\n",
    "\n",
    "This data is a derivative from the [COBRE](http://fcon_1000.projects.nitrc.org/indi/retro/cobre.html) sample found in the International Neuroimaging Data-sharing Initiative, originally released under Creative Commons - Attribution Non-Commercial.\n",
    "\n",
    "It comprises 10 preprocessed resting-state fMRI selected from 72 patients diagnosed with schizophrenia and 74 healthy controls (6mm isotropic, TR=2s, 150 volumes).\n",
    "\n",
    "Create a directory in the users home directory to store the downloaded data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_dir = op.expanduser('~/nilearn_data')\n",
    "\n",
    "if not op.exists(data_dir):\n",
    "    os.makedirs(data_dir)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Download the data (if not already downloaded):\n",
    "\n",
    "> **Note:** We use a method from [`nilearn`](https://nilearn.github.io/index.html) called [`fetch_cobre`](https://nilearn.github.io/modules/generated/nilearn.datasets.fetch_cobre.html) to download the fMRI data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "d = datasets.fetch_development_fmri(reduce_confounds=True, data_dir=data_dir, age_group='adult')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"clean-the-data\"></a>\n",
    "## Clean the data\n",
    "\n",
    "Regress confounds from the data and to spatially smooth the data with a gaussian filter of 10mm FWHM.\n",
    "\n",
    "> **Note:**\n",
    "> 1. We use [`clean_img`](https://nilearn.github.io/modules/generated/nilearn.image.clean_img.html) from the [`nilearn`](https://nilearn.github.io/index.html) package to regress confounds from the data\n",
    "> 2. We use [`smooth_img`](https://nilearn.github.io/modules/generated/nilearn.image.smooth_img.html) from the [`nilearn`](https://nilearn.github.io/index.html) package to spatially smooth the data\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a list of filenames for cleaned and smoothed data\n",
    "clean = [f.replace('.nii.gz', '_clean.nii.gz') for f in d.func]\n",
    "smooth = [f.replace('.nii.gz', '_clean_smooth.nii.gz') for f in d.func]\n",
    "\n",
    "# loop through each subject, regress confounds and smooth\n",
    "for img, cleaned, smoothed, conf in zip(d.func, clean, smooth, d.confounds):\n",
    "    print(f'{img}: regress confounds: ', end='')\n",
    "    image.clean_img(img, confounds=conf).to_filename(cleaned)\n",
    "    print(f'smooth.')\n",
    "    image.smooth_img(img, 10).to_filename(smoothed)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To run ```melodic``` we will need a brain mask in MNI152 space at the same resolution as the fMRI.  \n",
    "\n",
    "> **Note:**\n",
    "> 1. We use [`load_mni152_brain_mask`](https://nilearn.github.io/modules/generated/nilearn.datasets.load_mni152_brain_mask.html) from the [`nilearn`](https://nilearn.github.io/index.html) package to load the MNI152 mask\n",
    "> 2. We use [`resample_to_img`](https://nilearn.github.io/modules/generated/nilearn.image.resample_to_img.html) from the [`nilearn`](https://nilearn.github.io/index.html) package to resample the mask to the resolution of the fMRI \n",
    "> 3. We use [`math_img`](https://nilearn.github.io/modules/generated/nilearn.image.math_img.html) from the [`nilearn`](https://nilearn.github.io/index.html) package to binarize the resample mask\n",
    "> 4. The mask is plotted using [`plot_anat`](https://nilearn.github.io/modules/generated/nilearn.plotting.plot_anat.html) from the [`nilearn`](https://nilearn.github.io/index.html) package"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# load a single fMRI dataset (func0)\n",
    "func0 = nb.load(d.func[0].replace('.nii.gz', '_clean_smooth.nii.gz'))\n",
    "\n",
    "# load MNI153 brainmask, resample to func0 resolution, binarize, and save to nifti\n",
    "mask = datasets.load_mni152_brain_mask()\n",
    "mask = image.resample_to_img(mask, func0)\n",
    "mask = image.math_img('img > 0.5', img=mask)\n",
    "mask.to_filename(op.join(data_dir, 'brain_mask.nii.gz'))\n",
    "\n",
    "# plot brainmask to make sure it looks OK\n",
    "disp = plotting.plot_anat(image.index_img(func0, 0))\n",
    "disp.add_contours(mask, threshold=0.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"run-melodic\"></a>\n",
    "### Run ```melodic```\n",
    "\n",
    "Generate a command line string and run group ```melodic``` on the smoothed fMRI with a dimension of 10 components:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# generate melodic command line string\n",
    "melodic_cmd = f\"melodic -i {','.join(smooth)} --mask={op.join(data_dir, 'brain_mask.nii.gz')} -d 25 -v --sep_vn -o development_fmri.gica\"\n",
    "print(melodic_cmd)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# run melodic\n",
    "! {melodic_cmd}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"plot-group-ics\"></a>\n",
    "### Plot group ICs\n",
    "\n",
    "Now we can load and plot the group ICs generated by ```melodic```.\n",
    "\n",
    "This function will be used to plot ICs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def ortho_plot(d):\n",
    "\n",
    "    N = d.shape[-1]\n",
    "\n",
    "    fig, ax = plt.subplots(\n",
    "        int(np.ceil((N/2))),2, \n",
    "        figsize=(12, N)\n",
    "    )\n",
    "\n",
    "    for img, ax0 in zip(image.iter_img(d), ax.ravel()):\n",
    "        coord = plotting.find_xyz_cut_coords(img, activation_threshold=3.5)\n",
    "        plotting.plot_stat_map(img, cut_coords=coord, vmax=10, axes=ax0)\n",
    "        \n",
    "    return fig\n",
    "\n",
    "def lightbox_plot(d, k=5):\n",
    "    \n",
    "    N = d.shape[-1]\n",
    "\n",
    "    fig, ax = plt.subplots(N, 1, figsize=(k*2, N*2))\n",
    "\n",
    "    for img, ax0 in zip(image.iter_img(d), ax):\n",
    "        plotting.plot_stat_map(img, cut_coords=k, vmax=10, axes=ax0, display_mode='z')\n",
    "        \n",
    "    return fig\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hopefully you can see some familiar looking RSN spatial patterns:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load ICs\n",
    "ics = nb.load('development_fmri.gica/melodic_IC.nii.gz')\n",
    "\n",
    "# plot\n",
    "fig = ortho_plot(ics)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot\n",
    "fig = lightbox_plot(ics, k=7)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
