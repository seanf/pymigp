import setuptools

# from . import __version__

with open('requirements.txt', 'rt') as f:
    install_requires = [ln.strip() for ln in f.readlines()]

setuptools.setup(
    name="pymigp",
    version='0.1.0dev0',
    author="Sean Fitzgibbon",
    author_email="sean.fitzgibbon@ndcn.ox.ac.uk",
    description="Python implementation of MIGP",
    url="https://git.fmrib.ox.ac.uk/seanf/dhcp-neonatal-fmri-pipeline",
    install_requires=install_requires,
    packages=setuptools.find_packages(),
    include_package_data=True,
    python_requires='>=3.7',
    scripts=['pymigp/migp.py', 'pymigp/cli/migp_helper.py'],
)